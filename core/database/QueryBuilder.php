<?php

class QueryBuilder {

	/**
	 * Set Database Connect
	 */
	protected $pdo;

	/**
	 * Get Database Connect
	 * @param PDO $pdo
	 */
	public function __construct(PDO $pdo) {

		$this->pdo = $pdo;

	}

	/**
	 * Select all database results
	 * @param string $table
	 * @param  array  $columns
	 * @return array          
	 */
	public function select($table, $columns = ['*']) {


		$statement = $this->pdo->prepare("SELECT ".join(', ', $columns)." FROM $table");

		$statement->execute();

		return $statement->fetchAll(PDO::FETCH_CLASS);

	}

	/**
	 * Select Users Table Join Groups table
	 * @return array
	 */
	public function selectJoinUsersGroups() {

		$statement = $this->pdo->prepare("
			SELECT
			users.id, users.username, groups.group_name
			FROM 
			users
			LEFT JOIN groups
			ON users.group_id = groups.id
			ORDER BY users.created_at DESC
		");

		$statement->execute();

		return $statement->fetchAll(PDO::FETCH_CLASS);

	}

	/**
	 * Find Sub modules who belongs to module
	 * @param  int $module_id
	 * @return array            
	 */
	public function findSub($module_id)
	{
		$statement = $this->pdo->prepare("SELECT * FROM sub_modules WHERE module_id = :module_id");

		$statement->execute(array(
			":module_id"	=> $module_id
		));

		return $statement->fetchAll(PDO::FETCH_CLASS);
	}	

	/**
	 * Find Specific Database Table
	 * @param  string $table  
	 * @param  string $username
	 * @param  string $where
	 * @param  array  $columns
	 * @return array
	 */
	public function findName($table, $username, $where, $columns = ['*'])
	{

		$statement = $this->pdo->prepare("SELECT ".join(', ', $columns)." FROM $table WHERE $where = :$where");

		$statement->execute(array(
			":$where"	=> $username
		));

		return $statement->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Check Users Access 
	 * @param  int $userid  
	 * @param  int $groupid
	 * @return array
	 */
	public function checkUsersAccess($userid = null, $groupid = null)
	{
		$statement = $this->pdo->prepare("SELECT * FROM roles WHERE user_id = :user_id AND group_id = :group_id");

		$statement->execute(array(
			":user_id"		=> $userid,
			":group_id"		=> $groupid
		));

		return $statement->fetch(PDO::FETCH_ASSOC);
	}	


	/**
	 * Update roles table
	 * @param  int $module_id 
	 * @param  string $role_type 
	 * @param  int $sub_module
	 * @param  int $user_id    default null
	 * @param  int $group_id   default null
	 * @return array             
	 */
	public function updateType($module_id, $role_type, $sub_module, $user_id = null, $group_id = null)
	{
		$statement = $this->pdo->prepare("UPDATE roles SET module_id = :module_id, role_type = :role_type, sub_module = :sub_module WHERE user_id = :user_id AND group_id = :group_id");
		$statement->execute(array(
			":module_id"	=> $module_id,
			":role_type"	=> $role_type,
			":sub_module"	=> $sub_module,
			":user_id"		=> $user_id,
			":group_id"		=> $group_id
		));
	}	

	/**
	 * Insert Results to database
	 * @param  string $table     
	 * @param  array $parameters 
	 * @return string
	 */
	public function insert($table, $parameters) {

		$sql = sprintf(
			'insert into %s (%s) values (%s)',
			$table,
			implode(', ', array_keys($parameters)),
			':' . implode(', :', array_keys($parameters))
		);

		try {

		$statement = $this->pdo->prepare($sql);

		$statement->execute($parameters);

		} catch(Exception $e) {

			die('Something wrong... Try again!');

		}
	}

}