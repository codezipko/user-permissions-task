<?php 

namespace App\Models;

use App\Core\App;


class Module
{

	/**
	 * Call sub modules only with specific module id
	 * @param  int
	 * @return array
	 */
	public function getSubModules($module_id)
	{
		return App::get('database')->findSub($module_id);
	}	
}
