<?php 

namespace App\Models;

use App\Core\App;

class Role
{
	/**
	 * Get module name from POST
	 * @var string
	 */
	public $moduleName;


	/**
	 * Set module name
	 * @param string $moduleName
	 */
	public function __construct($moduleName)
	{
		$this->moduleName = $moduleName;
	}	

	/**
	 * Check If user have Full Access
	 * @param array $user
	 * @return boolean If have show true
	 */
	public function hasFullAccess($user)
	{

		$check = App::get('database')->checkUsersAccess( $user['id'], 0 ); 

		$checkGroup = App::get('database')->checkUsersAccess( 0, $user['group_id'] ); 

		$find = App::get('database')->findName('modules', $check['module_id'], 'id');

		if( $check['role_type'] == 'full_access' && $user['id'] == $check['user_id'] ) {

			if( $this->moduleName == $find['module_name'] || $this->accessInSubModule( $find['id'] ) ) {

				return var_dump(true);

			} else {

				return var_dump(false);

			}

		} else if ( $this->hasGroupAccess($user) ) {
			return var_dump(true);
		}

	}	

	/**
	 * Check If group have access
	 * @param  array  $user 
	 * @return boolean  Show true if have
	 */
	public function hasGroupAccess($user)
	{

		$check = App::get('database')->checkUsersAccess( 0, $user['group_id'] ); 

		$find = App::get('database')->findName('modules', $check['module_id'], 'id');

		$findSub = App::get('database')->findName('sub_modules', $this->moduleName, 'sub_name');

		if( $user['group_id'] == $check['group_id'] && $check['role_type'] == 'full_access' ) {

			if($this->moduleName == $find['module_name'] || $this->accessInSubModule($find['id'], $this->moduleName) ) {

				return true;

			}

		} else if ( $user['group_id'] == $check['group_id'] && $check['role_type'] == 'sub_access' ) {

			if( $findSub['id'] == $check['sub_module'] ) {

				return true;

			}

		}

	}

	/**
	 * Show if user have only access only 
	 * @param  [type]  $user [description]
	 * @return boolean       [description]
	 */
	public function hasSubAccess($user)
	{

		$check = App::get('database')->checkUsersAccess($user['id'], 0); 

		$findSub = App::get('database')->findName('sub_modules', $this->moduleName, 'sub_name');

		if( $check['role_type'] == 'sub_access' && $user['id'] == $check['user_id'] ) {

			if($findSub['id'] == $check['sub_module']) {

				var_dump(true);

			} else {
				var_dump(false);
			}

		}

	}

	/**
	 * If User have full access give access to sub class
	 * @param int $module_id
	 * @return boolean true if have access
	 */
	public function accessInSubModule( $module_id ) {

		$checkSub = App::get('database')->findSub( $module_id );

		foreach( $checkSub as $sub_model ) {

			if( $sub_model->sub_name == $this->moduleName ) {
				return true;
			}

		}

	}


}