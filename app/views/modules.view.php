<?php
require('partials/head.php'); 
?>

<div class="container" style="width: 800px;">
	<h1 class="text-center">Moduliai</h1>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
				<h4 class="text-center mt-2 mb-1">Moduliai</h4>
					<div class="row">
						<?php foreach($modules as $module) : ?>
							<div class="col-md-3 card-header ml-2 mb-2">
					 		<strong>
					 			<?= $module->module_name; ?>
					 		</strong>
					 		<?php 
					 			foreach ( $sub_modules->getSubModules($module->id) as $sub_module ) :
					 		?>
					 		<span class="d-block">
					 			<?= $sub_module->sub_name; ?>
					 		</span>
					 		<?php endforeach; ?>
					 		</div>
					 	<?php endforeach; ?>
					</div>
				</div>
				<div style="padding: 10px;">
				<span class="text-center">Pridėti modulį</span>
				<form class="mt-2" action="addModules" method="POST">
						<div class="form-group">
							<input type="text" name="module_name" class="form-control" placeholder="Modulio pavadinimas..." required>
						</div>
						<button type="submit" class="btn btn-sm btn-primary">Sukurti</button>
			    	</form>
			    </div>
			    <h4 class="text-center">Pridėti sub modulį</h4>
				<div style="padding: 10px;">
				<form class="mt-2" action="addSubModules" method="POST">
						<div class="form-group">
							<input type="text" name="sub_module_name" class="form-control" placeholder="Sub Modulio pavadinimas..." required>
						</div>
						<div class="form-group">
							<label for="module_id">Sub modulio pavadinimas</label>
							<select class="form-control" name="module_id">
								<?php foreach($modules as $module) : ?>
								<option value="<?= $module->id; ?>"><?= $module->module_name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<button type="submit" class="btn btn-sm btn-primary">Sukurti</button>
			    	</form>
			    </div>
			</div>
		</div>
	</div>
</div>


<?php require('partials/footer.php'); ?>