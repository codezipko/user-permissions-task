<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Egroup Užduotis</title>
	<!-- Styles -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">

</head>
<body>
<header>
	<?php 
		date_default_timezone_set('Europe/Vilnius');
	?>
<div class="text-center">
	<ul class="d-inline" style="list-style: none;">
		<li><a href="/egroup/">Vartotojai</a></li>
	<?php
	if( $_SERVER['REQUEST_URI'] == "/egroup/") { ?>
		<li><a href="index.php/groups">Vartotojų grupės</a></li>
		<li><a href="index.php/modules">Moduliai</a></li>
	<?php } else { ?>
		<li><a href="groups">Vartotojų grupės</a></li>
		<li><a href="modules">Moduliai</a></li>
	<?php 
	}
	?>
	</ul>
</div>
</header>