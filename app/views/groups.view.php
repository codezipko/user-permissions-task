<?php
require('partials/head.php'); 
?>

<div class="container" style="width: 800px;">
	<h1 class="text-center">Vartotojų grupės</h1>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<h4 class="text-center mt-2 mb-1">Grupės</h4>
				<div class="card-body">
					<ul class="list-group d-inline">
						<?php foreach($groups as $group) : ?>
					 		<li class="d-inline list-group-item text-center">
					 			<?= $group->group_name; ?>
					 		</li>
					 	<?php endforeach; ?>
					</ul>
					
				</div>
				<div style="padding: 10px;">
				<form class="mt-2" action="addGroup" method="POST">
						<div class="form-group">
							<input type="text" name="group_name" class="form-control" placeholder="Grupės pavadinimas..." required>
						</div>
						<button type="submit" class="btn btn-sm btn-primary">Sukurti</button>
			    	</form>
			    </div>
			</div>
		</div>
	</div>
</div>


<?php require('partials/footer.php'); ?>