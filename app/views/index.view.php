<?php
require('partials/head.php'); 
?>
</div>
<div class="container" style="width: 800px;">
	<h5 class="text-center">Patikrinkite ar vartotojas turi prieigą prie modulių.</h5>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
			<form action="index.php/find" method="GET" class="col-md-12">
				<div class="row">
					<div class="col-md-6 form-group">
						<label for="user_name">Vartojo vardas</label>
						<input type="text" name="user_name" class="form-control" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="search_module">Modulio pavadinimas</label>
						<input type="text" name="search_module" class="form-control" required>

					</div>
					<div class="form-group ml-3">
						<button type="submit" class="btn btn-sm btn-dark">Ieškoti</button>
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
	<div class="row mt-2">
		<div class="col-md-6">
			<div class="card">
				<h4 class="text-center mt-2 mb-1">Vartotojai</h4>
				<div class="card-body">
					<ul class="list-group">
						<?php foreach( $users as $user ) : ?>
					 		<li class="list-group-item text-center">
					 			<?= $user->username; ?>
					 			<span class="d-block">
					 				<?php 
					 				$group_name = $user->group_name;
					 				if( is_null($group_name) ) {
					 					echo 'Nėra';
					 				}
					 					echo $group_name;
									?>
					 				</span>
					 		</li>
					 	<?php endforeach; ?>
					</ul>
					<form class="mt-2" action="index.php/addUser" method="POST">
						<div class="form-group">
							<input type="text" name="username" class="form-control" placeholder="Vartotojo vardas..." required>
						</div>
						<div class="form-group">
							<select name="group_id" class="form-control">
								<option value="0">Pasirinkite grupę</option>
								<?php foreach( $groups as $group ) : ?>
									<option value="<?= $group->id; ?>"><?= $group->group_name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<button type="submit" class="btn btn-sm btn-primary">Pridėti</button>
			    	</form>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card">
				<h4 class="text-center mt-2 mb-1">Prieiga prie modulių</h4>
				<div class="card-body">
					<form class="mt-2" action="index.php/addRoles" method="POST">
						<div class="form-group">
							<select name="user_role" class="form-control">
								<option value="0">Pasirinkti vartotoją</option>
								<?php foreach( $users as $user ) : ?>
									<option value="<?php echo $user->id; ?>"><?= $user->username; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<label for="group_role">arba galite pridėti modulį ir grupei</label>
							<select name="group_role" class="form-control">
								<option value="0">Pasirinkti grupę</option>
								<?php foreach( $groups as $group ) : ?>
									<option value="<?php echo $group->id; ?>"><?= $group->group_name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>

						<div class="form-group">
							<select name="main_module" class="form-control">
								<option value="0">Pasirinkti modulį</option>
								<?php foreach( $modules as $module ) : ?>
									<option value="<?= $module->id; ?>"><?= $module->module_name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>

						<div class="form-group">
							<select name="sub_module" class="form-control">
								<option value="0">Prieiga prie visų sub modelių</option>
								<?php foreach( $sub_modules as $sub_module ) : ?>
									<option value="<?= $sub_module->id; ?>"><?= $sub_module->sub_name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<button type="submit" class="btn btn-sm btn-primary">Sukurti</button>
			    	</form>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require('partials/footer.php'); ?>