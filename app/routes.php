<?php

$router->get('', 'UsersController@index');
$router->post('addUser', 'UsersController@store');

$router->get('groups', 'GroupsController@index');
$router->post('addGroup', 'GroupsController@store');

$router->get('modules', 'ModulesController@index');
$router->post('addModules', 'ModulesController@store');
$router->post('addSubModules', 'ModulesController@storeSubModules');

$router->post('addRoles', 'RolesController@store');
$router->get('find', 'RolesController@find');
