<?php 

namespace App\Controllers;

use App\Core\App;
use App\Models\Role;

class RolesController {

	/**
	 * @var Call Role Class
	 */

	protected $role;

	public $user;

	/**
     * Give User or Group access into database
     * @return string
     */
	
	public function store()
	{

		$userID = $_POST['user_role']; // Get user id from POST

		$groupID = $_POST['group_role']; // Get group id from POST

		$moduleID = $_POST['main_module']; // Get module id from POST

		$subModuleID = $_POST['sub_module']; // Get sub module id from POST

		$roleType = $this->setRoleType($subModuleID); // Get Role Type

		if( $this->updateUserRole($userID, $groupID, $moduleID, $subModuleID, $roleType) ); // Update Roles database Table

		return redirect('/egroup/');

	}	

	/**
	 * Check if User or Group Have module and sub module access
	 * @return true If User Or Group have access
	 */
	public function find()
	{

		$username = $_GET['user_name']; // Get username from user_name input

		$moduleName = $_GET['search_module']; // Get module name or sub module name from search_module input

		$this->role = new Role($moduleName); // Call Role Class


		/**
		 * Check if username and module name set after click submit button
		 */
		if( isset($username) && isset($moduleName) ) {

			/**
			 * Find User By username from Database Table
			 * @var array
			 */
			$findUser = App::get('database')->findName('users', $_GET['user_name'], 'username');

			/**
			 * Check if username exists in database table
			 */
			if( $findUser['username'] != $username ) {
				var_dump('Tokio vartotojo nėra.');
			}

			/**
			 * Return Access Result
			 */
			return $this->getUsersRole($findUser, $this->role);

		}
	}

	/**
	 * Give user full access or sub access
	 * @param int $subModuleID 
	 */
	protected function setRoleType($subModuleID)
	{
		$type = "full_access";

		if( $subModuleID != 0 ) {
			$type = "sub_access";
		}

		return $type;
	}


	/**
	 * Insert or Update exists user roles
	 * @param  int $userID      Get user id
	 * @param  int $groupID     Get group id
	 * @param  int $moduleID    Get module id
	 * @param  int $subModuleID Get sub module id
	 * @param  string $roleType 	Get role type full_access or sub_access
	 */
	protected function updateUserRole( $userID, $groupID, $moduleID, $subModuleID, $roleType ) {

		/**
		 * Give groupID 0 if userid exists
		 */
		if( $userID != 0 ) {
			$groupID = 0;
		}

		/**
		 * Find User by userid 
		 * @var array
		 */
		$findUser = App::get('database')->findName('roles', $userID, 'user_id');


		/**
		 * Find User Group by groupid
		 * @var array
		 */
		$findGroup = App::get('database')->findName('roles', $groupID, 'group_id');

		if( $findUser['user_id'] == $userID && $groupID == 0 ) { // If username exists then update table

			$update = App::get('database')->updateType($moduleID, $roleType, $subModuleID, $userID, 0);

		} else if ( $findGroup['group_id'] == $groupID && $userID == 0 ) { // If group exists then update table

			$update = App::get('database')->updateType($moduleID, $roleType, $subModuleID, 0, $groupID);

		} else { // If user and group not exists then insert table to datase

			$insert = App::get('database')->insert('roles', [
					'user_id'		=> $userID,
					'group_id'		=> $groupID,
					'module_id'		=> $moduleID,
					'role_type'		=> $roleType,
					'sub_module'	=> $subModuleID
			]);

		}

		return $this;

	}



	/**
	 * Get result from Role Class
	 * @param  array $findUser
	 * @param  class $role
	 * @return boolean
	 */
	protected function getUsersRole($findUser, $role) {

		return $role->hasFullAccess($findUser) || $role->hasSubAccess($findUser);

	}

}