<?php 

namespace App\Controllers;

use App\Core\App;
use App\Models\Module;

class ModulesController {

	/**
	 * Module Class
	 * @var array
	 */
	public $module;


	/**
	 * Join module class
	 */
	public function __construct()
	{
		$this->module = new Module;
	}	

	/**
	 * Show Module View page
	 * @return array
	 */
	public function index()
	{
		$modules = App::get('database')->select('modules');

		$sub_modules = $this->module;

		return view('modules', compact('modules', 'sub_modules'));
	}	

	/**
	 * Insert Modules to Database
	 * @return array
	 */
	public function store()
	{
		$moduleName = $_POST['module_name'];

		$findName = App::get('database')->findName('modules', $moduleName, 'module_name', ['module_name']);

		if( strtolower($findName['module_name']) != strtolower($moduleName) ) {

			$insert = App::get('database')->insert('modules', [
					'module_name'		=> $moduleName
			]);

		}

		return redirect('modules');
	}	

	/**
	 * Insert sub modules to database
	 * @return array
	 */
	public function storeSubModules()
	{

		$subModuleName = $_POST['sub_module_name'];

		$findName = App::get('database')->findName('sub_modules', $subModuleName, 'sub_name', ['sub_name']);

		if( strtolower($findName['sub_name']) != strtolower($subModuleName) ) {

			$insert = App::get('database')->insert('sub_modules', [
					'sub_name'		=> $subModuleName,
					'module_id'		=> $_POST['module_id']
			]);

		}

		return redirect('modules');

	}	

}