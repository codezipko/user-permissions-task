<?php 

namespace App\Controllers;

use App\Core\App;
use App\Models\User;

class UsersController {
	
	/**
	 * Show Users View Page
	 * @return array
	 */
	public function index()
	{

		$users = App::get('database')->selectJoinUsersGroups();
		$users_role = App::get('database')->select('users');
		$modules = App::get('database')->select('modules');
		$sub_modules = App::get('database')->select('sub_modules');
		$groups = App::get('database')->select('groups');

		return view('index', compact('users', 'users_role', 'groups', 'modules', 'sub_modules'));

	}

	/**
	 * Insert Users to Database
	 * @return array
	 */
	public function store()
	{
		$findName = App::get('database')->findName('users', $_POST['username'], 'username', ['username']);

		if( strtolower($findName['username']) != strtolower($_POST['username']) ) {

			$insert = App::get('database')->insert('users', [
				'username'		=> $_POST['username'],
				'group_id'		=> $_POST['group_id'],
				'created_at'	=> date("Y-m-d H:i:s")
			]);

		}

		return redirect('/egroup/');
	}	

}