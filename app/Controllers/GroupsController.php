<?php 

namespace App\Controllers;

use App\Core\App;

class GroupsController {

	/**
	 * Show Group View page
	 * @return array
	 */
	public function index()
	{
		$groups = App::get('database')->select('groups');

		return view('groups', compact('groups'));
	}	

	/**
	 * Insert Groups to Database
	 * @return array
	 */
	public function store()
	{
		$groupname = $_POST['group_name'];

		$findName = App::get('database')->findName('groups', $groupname, 'group_name', ['group_name']);

		if( strtolower($findName['group_name']) != strtolower($groupname) ) {

			$insert = App::get('database')->insert('groups', [
					'group_name'		=> $groupname,
					'created_at'	=> date("Y-m-d H:i:s")
				]);

		}

		return redirect('groups');
	}	

}