=== Vartotoj� privilegij� sistema ===
Suk�r�: Mantas
Versija: 1.0.0

== Apra�ymas ==

Sistemoje j�s galite sukurti vartotoj� ar grup�. Taip pat galite kurti modelius, bei tam tikr� modeli� funkcijas. 
Naudotis modeliais ar j� funkcijomis, galite priskirti bet kur� i� sukurt� vartotoj� ar grupi�.

== Naudojimasis ==

1. Susikurkite savo vartotoj� arba galite pasirinkti i� jau esam� duomen� baz�je.
2. Galite susikurti savo norim� grup�, bei priskirti vartotojus � j�.
3. Vartotojas gali tur�ti tik vien� grup�. 
4. Grup� gali tur�ti neribot� kiek� vartotoj�.
5. Norint priskirti vartotojui prieig� prie modulio ar jo funkcij�, jums reikia susikurti modul� arba pasirinkti i� jau esam�.


== Instaliacija ==

1. Failus turite sukelti � savo pasirinkt� server�.
2. config.php faile, jums reik�s �vesti savo duomen� baz�s duomenis.
3. egroup_task.sql fail�, jums reikia importuoti � j�s� duomen� baz�s server�.

= 1.0.0 =
Pirma svetain�s versija
